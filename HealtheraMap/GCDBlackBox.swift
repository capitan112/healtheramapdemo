//
//  GCDBlackBox.swift
//  HealtheraMap
//
//  Created by Капитан on 20.01.17.
//  Copyright © 2017 OleksiyCheborarov. All rights reserved.
//

import Foundation


func performUIUpdatesOnMain(_ updates: @escaping () -> Void) {
    DispatchQueue.main.async {
        updates()
    }
}

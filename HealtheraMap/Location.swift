//
//  Location.swift
//  HealtheraMap
//
//  Created by Капитан on 20.01.17.
//  Copyright © 2017 OleksiyCheborarov. All rights reserved.


import Foundation
import UIKit
import MapKit

class Location: NSObject, MKAnnotation {
    let placeType: String
    let title: String?
    let coordinate: CLLocationCoordinate2D
    let placeId: String
    let currentDistance: Double

    init(placeType: String, title: String, placeId: String, coordinate: CLLocationCoordinate2D, currentDistance: Double ) {
        self.placeType = placeType
        self.title = title
        self.coordinate = coordinate
        self.placeId = placeId
        self.currentDistance = currentDistance
    }
}

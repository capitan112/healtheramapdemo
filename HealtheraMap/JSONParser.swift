//
//  JSONParser.swift
//  InfiniteTableViewScrollingTask
//
//  Created by Alexey Chebotarev on 3/16/16.
//  Copyright © 2016 Alexey Chebotarev. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class JSONParser : NSObject {
    
    private var locations = [Location]()
    private var pharmacy:Pharmacy?
    
    // MARK: Initializers
    private override init() {
        super.init()
    }
    
    // MARK: Shared Instance
    class func sharedInstance() -> JSONParser {
        struct Singleton {
            static var sharedInstance = JSONParser()
        }
        return Singleton.sharedInstance
    }
    
    // MARK: Parsed Data
    func parseDataLocation(_ data: Data?) -> [Location]? {
        var parsedResult: [String:  Any]
        
        do {
            parsedResult = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:Any]
            let data = RequestConstant.LocationKeys.Data
            
            guard let JSONlocations = parsedResult[data] as? [NSDictionary] else {
                return nil
            }
            
            let kplaceType = RequestConstant.LocationKeys.PlaceType
            let kplaceTitle = RequestConstant.LocationKeys.PlaceTitle
            let kplaceId = RequestConstant.LocationKeys.PlaceId
            let klong = RequestConstant.LocationKeys.Long
            let klat = RequestConstant.LocationKeys.Lat
            let kcurrentDistance = RequestConstant.LocationKeys.CurrentDistance
            
            for location in JSONlocations {
                let placeType = location[kplaceType] as! String
                let placeTitle = location[kplaceTitle] as! String
                let placeId = location[kplaceId] as! String
                let long = (location[klong] as! NSString).doubleValue
                let lat = (location[klat] as! NSString).doubleValue
                let currentDistance = (location[kcurrentDistance]  as! NSString).doubleValue
                
                let pharmacyLocation = Location(placeType: placeType, title: placeTitle, placeId: placeId, coordinate:  CLLocationCoordinate2D(latitude: lat, longitude: long), currentDistance: currentDistance)
                self.locations.append(pharmacyLocation)
                
            }
        } catch {
            print ("Could not parse the data as JSON: '\(data)'")
        }
        
        return locations
    }
    
    func parsePharmacy(_ data: Data?) -> Pharmacy? {
        var parsedResult: [String:Any]

        do {
            parsedResult = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:Any]
            let data = RequestConstant.PharmaciesKeys.Data
            
            
            guard let JSONpharmacyArray = parsedResult[data] as? [NSDictionary] else {
                return nil
            }
            
            let kcoord_lat = RequestConstant.PharmaciesKeys.CoordLat
            let kcoord_long = RequestConstant.PharmaciesKeys.CoordLong
            let kaddress = RequestConstant.PharmaciesKeys.Address
            let kpharmacyContact = RequestConstant.PharmaciesKeys.PharmacyContact
            let kgroup_id = RequestConstant.PharmaciesKeys.GroupId
            let kpharmacy_email = RequestConstant.PharmaciesKeys.PharmacyEmail
            let kpharmacy_icon = RequestConstant.PharmaciesKeys.PharmacyIcon
            let kpharmacy_id = RequestConstant.PharmaciesKeys.PharmacyId
            let kpharmacy_name = RequestConstant.PharmaciesKeys.PharmacyName
            let kpharmacy_unique = RequestConstant.PharmaciesKeys.PharmacyUnique
            let kpostcode = RequestConstant.PharmaciesKeys.Postcode
            let ktelephone = RequestConstant.PharmaciesKeys.Telephone
            let kpharmacy_timezone = RequestConstant.PharmaciesKeys.PharmacyTimezone
            let kdisallowed_message = RequestConstant.PharmaciesKeys.DisallowedMessage
         
            for JSONpharmacy in JSONpharmacyArray {
                let coord_lat = (JSONpharmacy[kcoord_lat] as! NSString).doubleValue
                let coord_long = (JSONpharmacy[kcoord_long] as! NSString).doubleValue
                let address = JSONpharmacy[kaddress] as! String
                let pharmacyContact = JSONpharmacy[kpharmacyContact]  as! String
                let group_id = JSONpharmacy[kgroup_id]  as! String
                
                
                let pharmacy_email = JSONpharmacy[kpharmacy_email] as! String
                
                var pharmacy_icon: UIImage? = nil
                if let icon = JSONpharmacy[kpharmacy_icon] as? UIImage {
                    pharmacy_icon = icon
                }
                
                let pharmacy_id = JSONpharmacy[kpharmacy_id] as! String
                let pharmacy_name = JSONpharmacy[kpharmacy_name] as! String
                var pharmacy_unique:String?
                if let pharmacy_un = JSONpharmacy[kpharmacy_unique] as? String {
                    pharmacy_unique = pharmacy_un
                }
                
                let postcode = JSONpharmacy[kpostcode] as! String
                let telephone = JSONpharmacy[ktelephone] as! String
                
                var pharmacy_timezone: String?
                if let pharmacy_zone = JSONpharmacy[kpharmacy_timezone] as? String {
                    pharmacy_timezone = pharmacy_zone
                }
                
                var disallowed_message: String?
                if let disallowed_mess = JSONpharmacy[kdisallowed_message] as? String {
                    disallowed_message = disallowed_mess
                }

                self.pharmacy = Pharmacy(coordinate:  CLLocationCoordinate2D(latitude: coord_lat, longitude: coord_long), address: address, pharmacyContact: pharmacyContact, group_id: group_id, pharmacy_email: pharmacy_email, pharmacy_icon: pharmacy_icon, pharmacy_id: pharmacy_id, pharmacy_name: pharmacy_name, pharmacy_unique: pharmacy_unique, postcode: postcode, telephone: telephone, pharmacy_timezone: pharmacy_timezone, disallowed_message: disallowed_message)
            }

        } catch {
            print ("Could not parse the data as JSON: '\(data)'")
        }
        
        return self.pharmacy
    }
}

//
//  Pharmacy.swift
//  HealtheraMap
//
//  Created by Капитан on 20.01.17.
//  Copyright © 2017 OleksiyCheborarov. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class Pharmacy {
    var coordinate: CLLocationCoordinate2D
    let address: String
    let pharmacyContact: String
    let group_id: String
    let pharmacy_email: String
    let pharmacy_icon: UIImage?
    let pharmacy_id: String
    let pharmacy_name: String
    let pharmacy_unique: String?
    let postcode: String
    let telephone: String
    let pharmacy_timezone: String?
    let disallowed_message: String?

    init(coordinate: CLLocationCoordinate2D, address: String, pharmacyContact: String, group_id :String, pharmacy_email:String,pharmacy_icon:UIImage?,pharmacy_id: String,pharmacy_name: String, pharmacy_unique:String?, postcode:String,telephone:String, pharmacy_timezone:String?, disallowed_message: String?) {
        
        self.coordinate = coordinate
        self.address = address
        self.pharmacyContact = pharmacyContact
        self.group_id = group_id
        self.pharmacy_email = pharmacy_email
        self.pharmacy_icon = pharmacy_icon
        self.pharmacy_id = pharmacy_id
        self.pharmacy_name = pharmacy_name
        self.pharmacy_unique = pharmacy_unique
        self.postcode = postcode
        self.telephone = telephone
        self.pharmacy_timezone = pharmacy_timezone
        self.disallowed_message = disallowed_message
    }
    
}

//
//  ViewController.swift
//  HealtheraMap
//
//  Created by Капитан on 20.01.17.
//  Copyright © 2017 OleksiyCheborarov. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    let cambridgeLocation = CLLocationCoordinate2D(latitude: 52.2053, longitude: 0.1218)
    private var locations = [Location]()

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        mapView.centerCoordinate = CLLocationCoordinate2DMake(cambridgeLocation.latitude, cambridgeLocation.longitude);
        
        let currentRegion = MKCoordinateRegion(center: cambridgeLocation, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
        mapView.region = currentRegion
        
        performUIUpdatesOnMain {
            LoadingIndicatorView.show("Loading")
        }
        
        loadLocations({ locations in
            for location in locations {
                DispatchQueue.main.async {
                    self.mapView.addAnnotation(location)
                }
            }
            performUIUpdatesOnMain {
                LoadingIndicatorView.hide()
            }
        })
    }
    
    // MARK: - Map Kit delegate
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        let identifier = "Location"
        if annotation is Location {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            
            if annotationView == nil {

                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView!.canShowCallout = true
    
                let btn = UIButton(type: .detailDisclosure)
                annotationView!.rightCalloutAccessoryView = btn
            } else {
                annotationView!.annotation = annotation
            }
            
            return annotationView
        }
        return nil
    }
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Location

        performUIUpdatesOnMain {
            LoadingIndicatorView.show()
        }

        self.loadPharmacyWith (id: location.placeId, { pharmacy in
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = NSTextAlignment.left
            
            let placeName = pharmacy.postcode + "\r\n" + pharmacy.address
            let headerText = NSMutableAttributedString(
                string: placeName,
                attributes: [
                    NSParagraphStyleAttributeName: paragraphStyle,
                    NSFontAttributeName : UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline),
                    NSForegroundColorAttributeName : UIColor.black
                ]
            )

            let placeInfo = "Contact: " + pharmacy.pharmacyContact + "\r\n" + "\r\n" + "Phone: " + pharmacy.telephone + "\r\n" + "e-mail: "  + pharmacy.pharmacy_email
            
            let messageText = NSMutableAttributedString(
                string: placeInfo ,
                attributes: [
                    NSParagraphStyleAttributeName: paragraphStyle,
                    NSFontAttributeName : UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote),
                    NSForegroundColorAttributeName : UIColor.black
                ]
            )
        
            let alertVC = UIAlertController(title: placeName, message: placeInfo, preferredStyle: .alert)
            alertVC.addAction(UIAlertAction(title: "OK", style: .default))

            alertVC.setValue(headerText, forKey: "attributedTitle")
            alertVC.setValue(messageText, forKey: "attributedMessage")
            
            performUIUpdatesOnMain {
                LoadingIndicatorView.hide()
                self.present(alertVC, animated: true)
            }
        })
    }
    
    // MARK: - Loading data from Network
    
    private func loadPharmacyWith(id: String, _ completions: @escaping (Pharmacy) -> ()) {
        ClientLoader.sharedInstance().loadPharmacyDetails(pharmacyID: id, completionHandler: { data, error in
        
            if error != nil {
                self.alertMessage("Error on loading", message: "Cann't load source data")
                print("Cann't load source data")
                
                performUIUpdatesOnMain {
                    LoadingIndicatorView.hide()
                }
                return
            } else {
                let pharmacy = JSONParser.sharedInstance().parsePharmacy(data)
                completions(pharmacy!)
            }
        })
    }

    private func loadLocations(_ completions: @escaping ([Location]) -> ()) {
        ClientLoader.sharedInstance().loadLocations({ data, error in
            if error != nil {
                self.alertMessage("Error on loading", message: "Cann't load source data")
                print("Cann't load source data")

                performUIUpdatesOnMain {
                    LoadingIndicatorView.hide()
                }
                
                return
            } else {
                let locations = JSONParser.sharedInstance().parseDataLocation(data)
                completions(locations!)
            }
        })
    }
    
    // MARK: - Alert
    
    private func alertMessage(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//
//  RequestConstant.swift
//  HealtheraMap
//
//  Created by Капитан on 20.01.17.
//  Copyright © 2017 OleksiyCheborarov. All rights reserved.

import Foundation
import UIKit


struct RequestConstant {
    
    // MARK: Server
    
    struct Server {
        static let APIScheme = "https"
        static let APIHost = "core.healthera.co.uk"
        static let APILocations = "/locations?lat=52.2053&long=0.1218&radius=10"
        static let APIPharmacies = "/pharmacies?pharmacy_id="
        static let APIToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0IjoiZjJjY2QyMTAtZGU3OC0xMWU2LWEyNWEtNzUxYjk5YTUwODc4IiwidSI6IjBmMmMwMTUxLWE4MjQtMTFlNi1hODg1LWY1ZGUwMGE1NGU0YyIsImciOlsiMGYyYzAxNTItYTgyNC0xMWU2LWI0ZjYtNzk1ZWNkNzIzYmM1Il0sInAiOnsiYyI6eyJhZGgiOjI1NSwiYXBwIjoyNTUsImJ1YyI6MjU1LCJkaXMiOjI1NSwiZ3JvIjoyNTUsImxvYyI6MjU1LCJtZXMiOjI1NSwicGhhIjoyNTUsInJlbSI6MjU1LCJzZXIiOjI1NSwidGFrIjoyNTUsInRociI6MjU1LCJ0b2siOjI1NSwidXNlIjoyNTV9LCJzIjp7ImxhYiI6MjU1LCJzdG4iOjI1NSwic3RzIjoyNTV9fSwiaWF0IjoxNDg0ODUyMTczLCJleHAiOjE0OTI2MjgxNzMsImF1ZCI6ImNvcmUiLCJpc3MiOiJhcGkwMSJ9.EIyDoiBVr8aPwsEsB5ejDALITmNP6FNWXScmNtaVv9Y"
    }
    
    enum Methods: String {
        case GET = "GET"
        case POST = "POST"
    }
    
    // keys for Locations
    
    struct LocationKeys {
        static let Data = "data"
        static let PlaceType = "place_type"
        static let PlaceTitle = "place_title"
        static let PlaceId = "place_id"
        static let Long = "long"
        static let Lat = "lat"
        static let CurrentDistance = "current_distance"
    }
    
    // keys for Pharmacies

    struct PharmaciesKeys {
        static let Data = "data"
        static let CoordLat = "coord_lat"
        static let CoordLong = "coord_long"
        static let Address = "address"
        static let PharmacyContact = "pharmacy_contact"
        static let GroupId = "group_id"
        static let PharmacyEmail = "pharmacy_email"
        static let PharmacyIcon = "pharmacy_icon"
        static let PharmacyId = "pharmacy_id"
        static let PharmacyName = "pharmacy_name"
        static let PharmacyUnique = "pharmacy_unique"
        static let Postcode = "postcode"
        static let Telephone = "telephone"
        static let PharmacyTimezone = "pharmacy_timezone"
        static let DisallowedMessage = "disallowed_message"
    }
}

//
//  ViewController.swift
//  HealtheraMap
//
//  Created by Капитан on 20.01.17.
//  Copyright © 2017 OleksiyCheborarov. All rights reserved.

import Foundation

class ClientLoader : NSObject {

    let session = URLSession.shared

    // MARK: Initializers
    
    private override init() {
        super.init()
    }
    
    // MARK: Shared Instance
    
    public class func sharedInstance() -> ClientLoader {
        struct Singleton {
            static var sharedInstance = ClientLoader()
        }
        return Singleton.sharedInstance
    }
    
     // MARK: GET
    
    public func loadPharmacyDetails(pharmacyID:String, completionHandler: @escaping (_ result: Data?, _ error: NSError?) -> Void ) {
        let fullPathPharmacyID =  RequestConstant.Server.APIPharmacies + pharmacyID
        loadJSONFromAPI(stringPath: fullPathPharmacyID, withToken: true, httpMethod: RequestConstant.Methods.GET, completionHandler: completionHandler)
    }
    
    public func loadLocations( _ completionHandler: @escaping (_ result: Data?, _ error: NSError?) -> Void ) {
        loadJSONFromAPI(stringPath: RequestConstant.Server.APILocations, withToken: false, httpMethod:RequestConstant.Methods.GET, completionHandler: completionHandler)
    }
    
    private func loadJSONFromAPI(stringPath: String, withToken: Bool, httpMethod: RequestConstant.Methods, completionHandler: @escaping (_ result: Data?, _ error: NSError?) -> Void ) {
        let url = NSURL(string: stringPath, relativeTo:baseURL())!
        var request = URLRequest(url: url as URL)
        
        request.httpMethod = httpMethod.rawValue
        
        if (withToken) {
            request.setValue("\(RequestConstant.Server.APIToken)", forHTTPHeaderField: "token")
        }
        
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response:URLResponse?, error:Error?) in
            completionHandler (data, error as NSError?)
        })
        
        task.resume()
    }
    
    private func baseURL() -> URL {
        var components = URLComponents()
        components.scheme = RequestConstant.Server.APIScheme
        components.host = RequestConstant.Server.APIHost

        return components.url!
    }
}

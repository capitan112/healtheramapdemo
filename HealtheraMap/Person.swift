//
//  Article.swift
//  InfiniteTableViewScrollingTask
//
//  Created by Alexey Chebotarev on 3/15/16.
//  Copyright © 2016 Alexey Chebotarev. All rights reserved.
//

import Foundation
import UIKit

class Location {
    let fullName : String
    let birthDate : String
    let photoURL: URL?
    let wikiURLArray:[NSDictionary]?
    let decription: String
    var image = UIImage(named: "placeholder")
    var state: ImageState
    
    init(fullName : String, birth_date : String, photoURL:URL?, wikiURLArray:[NSDictionary]?,  decription: String,  state: ImageState) {
        self.fullName = fullName
        self.birthDate = birth_date
        self.photoURL = photoURL
        self.wikiURLArray = wikiURLArray
        self.decription = decription
        self.state = state
    }
}

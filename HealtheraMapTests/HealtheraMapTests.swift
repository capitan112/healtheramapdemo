//
//  HealtheraMapTests.swift
//  HealtheraMapTests
//
//  Created by Капитан on 20.01.17.
//  Copyright © 2017 OleksiyCheborarov. All rights reserved.
//

import XCTest
@testable import HealtheraMap

class HealtheraMapTests: XCTestCase {
    var viewController: ViewController!
    
    override func setUp() {
        super.setUp()
        viewController = ViewController()
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        viewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            _ = self.viewController.view
        }
    }
    
}
